<?php
/**
 * userInfo.php
 *
 * user information file
 *
 * @version    1.1 2018-10-20
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Start Session
session_start();

// check user login
if (empty($_SESSION['user_id'])) {
    header("Location: index.php");
}

// Application library ( with DemoLib class )
include 'library.php';
$app = new Library();

$user = $app->userDetails($_SESSION['user_id']); // get user details
$country = $app->getCountry($user->country);
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">

<title>User information</title>
</head>
<body>
	<div class="container-fluid" style="height:100%; background-color: lightcyan;">
		<div style="height: 10%"></div>
		<div class="card">
			<div class="card-header" style="font-weight:bold; background-color: lavender;">User information</div>
			<div class="card-body">
				<table>
					<tr>
						<td>Name:</td>
						<td><?php echo $user->fullname; ?></td>
					</tr>
					<tr>
						<td>User name:</td>
						<td><?php echo $user->username; ?></td>
					</tr>
					<tr>
						<td>Birthday:</td>
						<td><?php echo $user->birthday; ?></td>
					</tr>
					<tr>
						<td>Email:</td>
						<td><?php echo $user->email; ?></td>
					</tr>
					<tr>
						<td>Phone number:</td>
						<td><?php echo $user->phonenumber; ?></td>
					</tr>
					<tr>
						<td>Country:</td>
						<td><?php echo $country; ?></td>
					</tr>
				</table>
				</div>
				<div class="card-footer" style="background-color: lavender;">
				<p>
					<a href="updateUserInfo.php" class="btn btn-outline-warning" style="width: 45%; margin-top: 3px; margin-left: 55%; font-weight: bold;">Update</a>
				</p>

			</div>
		</div>
	</div>
</body>

</html>