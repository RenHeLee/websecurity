<?php

/**
 * login.php
 *
 * Login system
 *
 * @version    1.1 2018-10-20
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018 

 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Start Session
session_start();

// Application library ( with DemoLib class )
require 'library.php';
$app = new Library();

$login_error_message = '';

// check Login request
if (! empty($_POST['loginForm'])) {

    $userInput = trim($_POST['userInput']);
    $password = trim($_POST['password']);

    $user_id = $app->login($userInput, $password); // check user login
    if ($user_id > 0) {
        $_SESSION['user_id'] = $user_id; // Set Session
        header("Location: profile.php"); // Redirect user to the profile.php
    } else {
        $login_error_message = 'Invalid login details!';
    }
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">


<title>Pilot project of Web Security</title>
</head>

<body>
	<div class="container-fluid">
		<form action=index.php method="post">
			<h4>Sign in</h4>
		<?php
if ($login_error_message != "") {
    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $login_error_message . '</div>';
}
?>
		<table>
				<tr>
					<td><label for="userInput">Username / email: </label></td>
					<td><input id="userInput" name="userInput" type="text" required
						style="width: 130%"></td>
				</tr>
				<tr>
					<td><label for="password">Password: </label></td>
					<td><input id="password" name="password" type="password" required
						style="width: 130%"></td>
				</tr>
			</table>
			<input type="submit" value="Login" name="loginForm"
				class="btn btn-outline-success"
				style="width: 45%; margin-top: 3%; margin-left: 3%; font-weight: bold;" />
			<a href="resetPassword.php" class="btn btn-outline-danger"
				style="width: 45%; margin-top: 3%; margin-left: 3%; font-weight: bold;">Forgot
				password?</a> <a href="signup.php" class="btn btn-outline-primary"
				style="width: 94%; margin-top: 3%; margin-left: 3%; margin-right: 3%; font-weight: bold;">Sign
				up</a>
		</form>
	</div>
</body>