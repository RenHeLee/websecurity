<?php

/**
 * library.php
 *
 * library file
 *
 * @version    1.2 2018-10-20
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */
include 'database.php';

class Library
{

    /*
     * Register New User
     *
     * @param $fullName, $userName, $birthday, $email, $phoneNumber, $country, $password, $secureQuestion, $secureAnswer
     * @return $user_id
     */
    public function register($fullName, $userName, $birthday, $email, $phoneNumber, $country, $password, $secureQuestion, $secureAnswer)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("INSERT INTO websecurity.user
(fullname, username, birthday, email, phonenumber, country, hashedpassword, securequestion, secureanswer) 
VALUES (:fullName, :userName, :birthday, :email, :phoneNumber, :country, :hash_password, :secureQuestion, :hash_secureAnswer)");
            $query->bindParam(":fullName", $fullName, PDO::PARAM_STR);
            $query->bindParam(":userName", $userName, PDO::PARAM_STR);
            $query->bindParam(":birthday", $birthday, PDO::PARAM_STR);
            $query->bindParam(":email", $email, PDO::PARAM_STR);
            $query->bindParam(":phoneNumber", $phoneNumber, PDO::PARAM_STR);
            $query->bindParam(":country", $country, PDO::PARAM_STR);
            $hash_password = hash('sha256', $password);
            $query->bindParam(":hash_password", $hash_password, PDO::PARAM_STR);
            $query->bindParam(":secureQuestion", $secureQuestion, PDO::PARAM_STR);
            $hash_secureAnswer = hash('sha256', $secureAnswer);
            $query->bindParam(":hash_secureAnswer", $hash_secureAnswer, PDO::PARAM_STR);
            $query->execute();
            return $db->lastInsertId();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Check Username
     *
     * @param $username
     * @return boolean
     */
    public function isUsername($userName)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT user_id FROM websecurity.user WHERE username=:userName;");
            $query->bindParam(":userName", $userName, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Check Email
     *
     * @param $email
     * @return boolean
     */
    public function isEmail($email)
    {
        try {
            global $pdo;
            $db = $pdo;
            // $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $query = $db->prepare("SELECT user_id FROM websecurity.user WHERE email=:email");
            $query->bindParam("email", $email, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Check Password
     *
     * @param $password, $verifyPassword
     * @return boolean
     */
    public function isPassword($password, $verifyPassword)
    {
        try {
            if ($password != $verifyPassword) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Login
     *
     * @param $userInput, $password
     * @return $mixed
     */
    public function login($userInput, $password)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT user_id FROM websecurity.user WHERE (username=:userInput OR email=:userInput) AND hashedpassword=:hashedPassword");
            $query->bindParam(":userInput", $userInput, PDO::PARAM_STR);
            $hashedPassword = hash('sha256', $password);
            $query->bindParam(":hashedPassword", $hashedPassword, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->user_id;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get User Details
     *
     * @param $user_id
     * @return $mixed
     */
    public function userDetails($user_id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT user_id, fullname, username, birthday, email, phonenumber, country, hashedpassword FROM websecurity.user WHERE user_id=:user_id");
            $query->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                return $query->fetch(PDO::FETCH_OBJ);
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get user_id
     *
     * @param $userInput
     * @return $mixed
     */
    public function getUser_id($userInput)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT user_id FROM websecurity.user WHERE (username=:userInput OR email=:userInput)");
            $query->bindParam(":userInput", $userInput, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->user_id;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get Securequestion
     *
     * @param $user_id
     * @return $secureQuestion
     */
    public function getSecurequestion($user_id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT securequestion FROM websecurity.user WHERE user_id=:user_id");
            $query->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->securequestion;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get Secureanswer
     *
     * @param $user_id
     * @return $secureAnswer
     */
    public function getSecureanswer($user_id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT secureanswer FROM websecurity.user WHERE user_id=:user_id");
            $query->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->secureanswer;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Check hashed value
     *
     * @param $userAnswer, $systemAnswer(hashed)
     * @return boolean
     */
    public function isHashedValue($userAnswer, $systemAnswer)
    {
        $hashedUserAnswer = hash('sha256', $userAnswer);
        try {
            if ($hashedUserAnswer != $systemAnswer) {
                return true;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Update password
     *
     * @param $user_id, $password
     * @return void
     */
    public function updatePassword($user_id, $password)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("UPDATE websecurity.user SET hashedpassword = :hash_password WHERE user_id = :user_id");
            $query->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $hash_password = hash('sha256', $password);
            $query->bindParam(":hash_password", $hash_password, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Update User Information
     *
     * @param $fullName, $userName, $birthday, $email, $phoneNumber, $country
     * @return void
     */
    public function updateUser($user_id, $fullName, $userName, $birthday, $email, $phoneNumber, $country)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("UPDATE websecurity.user SET fullName = :fullName, userName = :userName, birthday = :birthday, email = :email, phoneNumber = :phoneNumber, country = :country WHERE  user_id = :user_id");
            $query->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $query->bindParam(":fullName", $fullName, PDO::PARAM_STR);
            $query->bindParam(":userName", $userName, PDO::PARAM_STR);
            $query->bindParam(":birthday", $birthday, PDO::PARAM_STR);
            $query->bindParam(":email", $email, PDO::PARAM_STR);
            $query->bindParam(":phoneNumber", $phoneNumber, PDO::PARAM_STR);
            $query->bindParam(":country", $country, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get Country Name
     *
     * @param $countryCode
     * @return $country
     */
    public function getCountry($countryCode)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT countries_name FROM websecurity.countries WHERE countries_iso_code = '" . $countryCode . "';");
            $query->execute();
            if ($query->rowCount() > 0) {
                $result = $query->fetch(PDO::FETCH_OBJ);
                return $result->countries_name;
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Upload post
     *
     * @param $user_id, $post, $time
     * @return void
     */
    public function uploadPost($user_id, $newpost, $posttime)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("INSERT INTO websecurity.post (user_id, newpost, posttime) VALUES (:user_id, :newpost, :posttime)");
            $query->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $query->bindParam(":newpost", $newpost, PDO::PARAM_STR);
            $query->bindParam(":posttime", $posttime, PDO::PARAM_STR);
            $query->execute();
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    /*
     * Get post
     *
     * @param $user_id
     * @return $post
     */
    public function getPost($user_id)
    {
        try {
            global $pdo;
            $db = $pdo;
            $query = $db->prepare("SELECT newpost, posttime FROM websecurity.post WHERE user_id=:user_id ORDER BY posttime DESC");
            $query->bindParam(":user_id", $user_id, PDO::PARAM_STR);
            $query->execute();
            if ($query->rowCount() > 0) {
                return $query->fetchAll();
            }
        } catch (PDOException $e) {
            exit($e->getMessage());
        }
    }

    function isMultidimensional($array)
    {
        return count($array) !== count($array, COUNT_RECURSIVE);
    }

    /*
     * Display post
     *
     * @param $username, $post
     * @return string
     */
    public function displayPost($username, $post)
    {
        $app = new Library();
        $str = '';
        if ($app->isMultidimensional($post)) {
            foreach ($post as $key) {
                $str .= "<div class=\"card\">
  <div class=\"card-header\" style=\"font-weight:bold; background-color: lavender;\">Posted by: $username</div>
  <div class=\"card-body\">$key[newpost]</div>
  <div class=\"card-footer\" style=\"font-weight:bold; background-color: lavender;\">When: $key[posttime]</div>
</div></br>";
            }
            return $str;
            ;
        } else {
            $str .= "<div class=\"card\">
  <div class=\"card-header\" style=\"font-weight:bold; background-color: lavender;\">Posted by: $username</div>
  <div class=\"card-body\">$post[newpost]</div>
  <div class=\"card-footer\" style=\"font-weight:bold; background-color: lavender;\">When: $post[posttime]</div>
</div></br>";
        }
    }
}