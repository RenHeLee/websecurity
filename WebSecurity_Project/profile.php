<?php
/**
 * profile.php
 *
 * profile file
 *
 * @version    1.1 2018-11-07
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018 
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Start Session
session_start();

// check user login
if (empty($_SESSION['user_id'])) {
    header("Location: index.php");
}

// Database connection
include 'database.php';
global $pdo;
$db = $pdo;

// Application library ( with DemoLib class )
include 'library.php';
$app = new Library();

$user = $app->userDetails($_SESSION['user_id']); // get user details

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Profile</title>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">

</head>
<body>
	<div class="container-fluid" style="height: 100%">
		<div class="row" style="height: 8%">
			<div class="col-sm-12"
				style="background-color: lavender; color: black; margin-left: 0px; font-size: 3em;">
				<p style="text-align: center; ">Markybook</p>
			</div>
		</div>
		<div class="row" style="height: 89%">
			<div class="col-sm-9"  style="background-color: lightcyan;">
			<iframe src="post.php" name="post" id="post" frameborder="0" scrolling="no" style="height: 100%; width: 100%;"></iframe></div>
			<div class="col-sm-3" style="background-color: lightcyan;">
			<iframe src="userInfo.php" name="userdata" id="userdata" frameborder="0" scrolling="no" style="height: 100%; width: 100%;"></iframe>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12"
				style="height: 3%; background-color: lavender; color: black; margin-bottom: 0px;">
				<p>
					Copyright (c) 2018 BIS20303 Web Security <a href="about.php">About</a>
					<a href="logout.php">Logout</a>
				</p>
			</div>
		</div>

	</div>
</body>

</html>