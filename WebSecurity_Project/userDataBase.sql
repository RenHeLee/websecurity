CREATE TABLE  `user` (
`user_id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
`fullname` VARCHAR( 100 ) NOT NULL ,
`username` VARCHAR( 50 ) NOT NULL ,
`birthday` DATE  NOT NULL,
`email` VARCHAR( 100 ) NOT NULL ,
`phonenumber` VARCHAR( 20 ) NOT NULL ,
`country` VARCHAR( 100 ) NOT NULL ,
`hashedpassword` VARCHAR( 250 ) NOT NULL,
`securequestion` VARCHAR( 2500 ) NOT NULL,
`secureanswer` VARCHAR( 500 ) NOT NULL
) ;