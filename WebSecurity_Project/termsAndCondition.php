<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Terms and conditions of Markybook</title>
</head>
<body>
	<div class="container-fluid"
		style="background-image: url('img/img_0001.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
		<div style="height: 15%; color: blue;">
			<h1 style="text-align: center;">Terms and conditions of Markybook</h1>
		</div>

		<div style="height: 80%;">
			<div class="card"
				style="border-color: #81D8D0; overflow-y: scroll; height: 90%; width: 50%; margin-left: auto; margin-right: auto; background: rgba(230, 230, 250, 0.85);">
				<div class="card-block"
					style="padding-left: 5%; padding-right: 5%; padding-top: 5%; padding-bottom: 5%;">
					<p>Markybook, Inc. (“Markybook”) is pleased to offer you access to
						this website (the “Site”), Markybook’s social networks, and other
						services that Markybook may from time to time provide from this
						website, subject to these Terms and Conditions of Use (the
						“Terms”). The term “Social Networks” encompasses a broad sweep of
						online activity, including participation in social networks such
						as Facebook, YouTube, Flickr, and Twitter, and interactive blogs,
						websites or listserves, some of which may be located on the Site.
						These Terms apply to all users of the Site and the Markybook
						Social Networks, including users who are also contributors of User
						Generated Content (as hereinafter defined).</p>
					<p>BY PROCEEDING BEYOND THE SITE HOME PAGE OR VIEWING OR POSTING
						CONTENT TO THE MARKYBOOK SOCIAL NETWORKS, YOU EXPRESS YOUR CONSENT
						TO, AGREEMENT WITH, AND UNDERSTANDING OF THESE TERMS. MARKYBOOK
						MAY, IN ITS SOLE DISCRETION, MODIFY, RESTRICT, CHANGE, OR
						OTHERWISE ALTER THESE TERMS, THE SITE, OR THE MARKYBOOK SOCIAL
						NETWORKS, IN WHOLE OR IN PART, IMPOSE LIMITS ON CERTAIN FEATURES
						ON THE SITE OR THE MARKYBOOK SOCIAL NETWORKS, OR RESTRICT YOUR
						ACCESS TO PART OR ALL OF THE SITE OR THE MARKYBOOK SOCIAL
						NETWORKS. BY CONTINUING TO ACCESS AND USE THE SITE AND/OR THE
						MARKYBOOK SOCIAL NETWORKS YOU WILL BE EVIDENCING YOUR CONSENT TO,
						AGREEMENT WITH, AND UNDERSTANDING OF, SUCH MODIFICATIONS, CHANGES
						OR ALTERATIONS. PLEASE REVIEW THESE TERMS REGULARLY AS THEY MAY BE
						MODIFIED FROM TIME TO TIME WITHOUT NOTICE TO YOU.</p>
					<h6>Markybook Generated Content</h6>
					<p>The copyright rights and other property rights to the Site and
						the Markybook Social Networks are owned by Markybook. All rights
						regarding the content posted to the Site and the Markybook Social
						Networks by Markybook are reserved by Markybook. The content on
						the Site may not be used or disseminated without the prior written
						consent of Markybook, except that you may download spec sheets
						solely for the purpose of doing business with Markybook. The Site
						and the Markybook Social Networks may contain text, images,
						audiovisual productions, opinions, statements, facts, articles, or
						other information created by Markybook or by third parties for
						Markybook. Such content is for your reference only and should not
						be relied upon by you for any purpose. Markybook is not
						responsible for the content’s accuracy and reliability.</p>
					<p>Some of the information provided by Markybook on the Site and
						the Markybook Social Networks may contain projections or other
						statements regarding future events or the future financial
						performance of Markybook that constitute “forward-looking
						statements” within meaning of the Securities Exchange Act of 1934,
						the Private Securities Litigation Reform Act of 1995 and other
						related laws that provide a “safe harbor” for forward-looking
						statements. These forward-looking statements, which may include
						statements that reflect our current views with respect to future
						events and financial performance, are identified by their use of
						such terms and phrases as “intend,” “goal,” “estimate,” “expect,”
						“project,” “projections,” “plans,” “anticipate,” “should,”
						“designed to,” “foreseeable future,” “believe,” “confident,”
						“think,” “scheduled,” “outlook,” “guidance” and similar
						expressions. This list of indicative terms and phrases is not
						intended to be all-inclusive. Readers are cautioned not to place
						undue reliance on these forward-looking statements, which speak
						only as of the date they were made. We are not undertaking any
						duty or obligation to update any forward-looking statements to
						reflect developments or information obtained after the date such
						forward-looking statements are made.</p>
					<p>Our actual results may differ significantly from the results
						discussed in forward-looking statements. Factors that might cause
						such a difference include, but are not limited to: (a) the general
						economic, political and competitive conditions in the markets
						where we operate; (b) changes in capital availability or costs,
						such as changes in interest rates, security ratings and market
						perceptions of the businesses in which we operate; (c) changes in
						the regulatory framework governing business generally, and the
						telecommunication services and equipment industry in particular,
						in the U.S. and other countries; (d) changes in authoritative
						generally accepted accounting principles or policies from such
						standard-setting bodies as the Financial Accounting Standards
						Board, the Public Company Accounting Oversight Board and the
						Securities and Exchange Commission (SEC); (e) the impact of
						corporate governance, accounting and securities law reforms by the
						United States Congress, the SEC or the New York Stock Exchange;
						(f) natural and man-made catastrophes; and (g) other factors
						discussed in the documents we file from time to time with the SEC,
						including specifically our most recently filed reports on Forms
						10-K, 10-Q and 8-K, as amended, to which we refer you for more
						information regarding our forward-looking statements and risk
						factors.</p>
					<p>Markybook may provide links to other websites as a convenience
						for its Site and Social Networks users. Markybook is not
						responsible for the content of these other websites, and Markybook
						does not endorse, warrant or guarantee the products or services
						described or offered in these other websites.</p>
					<h6>User Generated Content</h6>
					<p>You may submit content, including text, images, photographs and
						video to the Markybook Social Networks (collectively, “User
						Generated Content”), provided that you abide by these common sense
						rules:</p>
					<ul>
						<li style="font-style: italic;">If you post it, you own it</li>
						<p>Think twice before you publish. Would you want your family,
							friends, colleagues, or others to read or see the content you
							post? Keep in mind that even if original content can be deleted,
							the content that is shared and distributed through an array of
							channels can be reposted, searched, and found on the Internet.</p>
						<li style="font-style: italic;">Respect the law, including with
							regard to intellectual property</li>
						<p>It is critical that you show proper respect for the law
							generally, and specifically for the laws governing copyright and
							fair use of copyrighted material. You should not quote more than
							short excerpts from someone else’s work. It is good general
							practice to link to others’ work. Use appropriate symbols to mark
							the first appearance of trademarked terms in your content. Do not
							post images, music or other copyrighted content unless you own
							them or have permission from the owner to display such content in
							this context. Keep in mind that the laws may differ depending on
							where you live.</p>
						<li style="font-style: italic;">Treat others like you want to be
							treated</li>
						<p>Be respectful of others’ opinions and beliefs. Use common
							courtesy when posting content: refrain from abusive, obscene, or
							offensive language, images, or links. Do not post content from,
							or links to, Internet sites that feature sexual content,
							gambling, or that advocate intolerance of others; and do not
							create such content on Markybook’s Social Networks.</p>
						<li style="font-style: italic;">Do not post confidential or
							proprietary information</li>
						<p>By posting content to Markybook’s Social Networks, you are
							agreeing to allow anyone with access to Markybook’s Social
							Networks to access it and use it without restriction, except with
							regard to any applicable intellectual property rights that you or
							others may have in such content. You agree that you will not post
							or transmit material that is confidential or proprietary to you
							or a third-party.</p>
					</ul>
					<h6>Markybook’s Rights and Disclaimers</h6>
					<ul>
						<li style="font-style: italic;">User Generated Content</li>
						<ul>
							<li>You grant to Markybook a worldwide, non-exclusive, perpetual,
								irrevocable, royalty-free and fully-paid, transferable right
								(including the right to sublicense) to exercise all copyright,
								publicity, and moral rights with respect to any User Generated
								Content you post on the Markybook Social Networks in any media
								formats and through any media channels.</li>
							<li>Markybook expressly disclaims any liability or responsibility
								for the User Generated Content posted to its Social Networks,
								and such content does not necessarily represent the opinions or
								positions of Markybook.</li>
							<li>Markybook does not promise or guarantee that any User
								Generated Content posted on the Markybook Social Networks is
								correct or accurate, and Markybook does not necessarily agree
								with or endorse such content.</li>
							<li>Markybook retains the right to decline to post any User
								Generated Content, or remove any previously posted content on
								Markybook Social Networks in our sole discretion.</li>
							<li>You understand that when using the Site or the Markybook
								Social Networks, you will be exposed to User Generated Content
								from a variety of sources, and that Markybook is not responsible
								for the usefulness, safety, or intellectual property rights of
								or relating to such User Generated Content. You further
								understand and acknowledge that you may be exposed to User
								Generated Content that is inaccurate, offensive, indecent, or
								objectionable, and you agree to waive, and hereby do waive, any
								legal or equitable rights or remedies you have or may have
								against Markybook with respect thereto.</li>
						</ul>
						<li style="font-style: italic;">Markybook’s Social Networks</li>
						<ul>
							<li>Markybook reserves the right, but does not have the
								obligation, to monitor Markybook’s Social Networks.</li>
						</ul>
						<li style="font-style: italic;">General</li>
						<ul>
							<li>ALL CONTENT PROVIDED ON the SITE AND THE Markybook Social
								Networks IS PROVIDED “as is”, WITH ALL FAULTS, WITHOUT WARRANTY
								OF ANY KIND, EITHER express or implied, including, WITHOUT
								LIMITATION, THOSE OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
								PURPOSE AND noninfringement of any intellectual or other
								proprietary rights OR ARISING FROM A COURSE OF DEALING, USAGE,
								OR TRADE PRACTICE.</li>
							<li>Markybook AND ITS SUPPLIERS SHALL not be liable for ANY
								direct or indirect damages, including without limitation, lost
								profits OR REVENUES, COSTS OF REPLACEMENT GOODS, lost savings,
								LOSS OR DAMAGE TO DATA ARISING OUT OF THE USE OR INABILITY TO
								USE THE SITE OR THE MARKYBOOK SOCIAL NETWORKS OR ANY MARKYBOOK
								PRODUCT, DAMAGES RESULTING FROM THE USE OF OR RELIANCE ON THE
								CONTENT PRESENTED, or any incidental, special, or other economic
								consequential damages, even if MARKYBOOK OR ITS SUPPLIERS HAVE
								been advised of the possibility of such damages.</li>
							<li>By using the SITE AND THE Markybook Social Networks, you
								agree to indemnify AND HOLD MARKYBOOK HARMLESS against any AND
								ALL third-party action based UPon OR ARISING DIRECTLY OR
								INDIRECTLY FROM any content you post on the Markybook Social
								Networks.</li>
							<li>Markybook does not grant you any express or implied rights or
								licenses under any intellectual property rights.</li>
						</ul>
					</ul>
					<h6>Privacy</h6>
					<ul>
						<li>Markybook respects your personal privacy and the sensitivity
							of your corporate information. When you visit the Site, we may
							collect personally identifiable information that you choose to
							voluntarily disclose. We also may automatically collect certain
							website use information as you browse the Site. We may share your
							personal information with other companies and individuals that
							perform supporting functions in connection with orders for
							Markybook products (e.g., credit card processing), and with
							maintaining the Site and the Markybook Social Networks. Except as
							provided herein, we will not share your personal information or
							any website use information with any party unless required by
							law.</li>
						<li>Be considerate of others’ privacy, do not reference another
							without his or her permission.</li>
					</ul>
					<h6>Markybook Employees</h6>
					<ul>
						<li>If you are a Markybook employee, you must also follow the
							Markybook Employee Social Networking Guidelines.</li>
						<li>Content posted by Markybook employees is their own and does
							not necessarily represent the positions, strategies, or opinions
							of Markybook, unless such persons have been authorized by
							Markybook to speak on its behalf. The content on the Markybook
							Social Networks is provided for informational purposes only and
							is not meant to be an endorsement or representation by Markybook
							or any other party</li>
					</ul>
					<h6>Digital Millennium Copyright Act Notice and Takedown Procedure</h6>
					<ul>
						<li>If you believe that your copyright or other rights have been
							infringed, please provide Markybook’s Designated Agent
							(identified below) written notice with the following information:</li>
						<ul>
							<li>An electronic or physical signature of the person authorized
								to act on behalf of the owner of the copyright or other
								interest.</li>
							<li>A description of the copyrighted work or other work that you
								claim has been infringed.</li>
							<li>A description of where the material that you claim is
								infringing is located on the Site or the Markybook Social
								Networks.</li>
							<li>Your address, telephone number, and email address.</li>
							<li>A written statement by you that you have a good faith belief
								that the disputed use is not authorized by the copyright owner,
								its agent, or the law.</li>
							<li>A statement by you, made under penalty of perjury, that the
								above information in your notice is accurate and that you are
								the copyright owner or are authorized to act on the copyright
								owner’s behalf.</li>
						</ul>
						<li>Markybook’s Designated Agent for notice of claims of copyright
							infringement can be reached as follows:</li>
						<p>
							Markybook, Inc.</br>Universiti Tun Hussein Onn Malaysia,</br>86400,
							Parit Raja, Batu Pahat, Johor.</br>Attn: General Counsel</br>Email:
							bis20303@uthm.edu.my
						</p>
						<li>If the disputed materials were posted by a third party
							identifiable through reasonable efforts, we will provide
							reasonable notice to the third party of the charge. If the third
							party responds with a valid counter-notification, we will provide
							you with a copy so that you may take any other steps you may
							consider appropriate.</li>
					</ul>

					<script language="javascript">
function goBack() {
    window.history.back();
}
</script>

					<button onclick="goBack()" class="btn btn-outline-primary"
						style="width: 32%; font-weight: bold;">Back</button>
				</div>
			</div>
		</div>
		<div style="height: 5%; color: white;">
			<p>
				Copyright (c) 2018 BIS20303 Web Security <a href="about.php">About</a>
			</p>
		</div>
	</div>


</body>
</html>