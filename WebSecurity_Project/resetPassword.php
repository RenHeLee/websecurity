<?php
/**
 * resetPassword.php
 *
 * resetPassword file
 *
 * @version    1.1 2018-11-15
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018 
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Start Session
session_start();

// Application library ( with DemoLib class )
require 'library.php';
$app = new Library();

$resetPassword_error_message = '';

// check Reset request
if (! empty($_POST['resetPasswordForm'])) {
    if ($app->isUsername($_POST['userInput']) || $app->isEmail($_POST['userInput'])) {
        $user_id = $app->getUser_id($_POST['userInput']); // get user_id
        if ($user_id > 0) {
            $_SESSION['user_input'] = $_POST['userInput']; // Set Session
            header("Location: changePassword.php"); // Redirect user to the profile.php
        }
    } else {
        $resetPassword_error_message = 'Invalid reset details!';
    }
}
?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Forget password</title>
</head>
<body>
	<div class="container-fluid"
		style="background-image: url('img/img_0004.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
		<div style="height: 15%; color: blue;">
			<h1 style="text-align: center;">Forget Password</h1>
		</div>

		<div style="height: 80%;">
			<div class="card"
				style="border-color: #81D8D0; width: 500px; margin-left: auto; margin-right: auto; margin-top: auto; margin-bottom: auto; background: rgba(230, 230, 250, 0.7);">
				<div class="card-block"
					style="padding-left: 5%; padding-right: 5%; padding-top: 5%; padding-bottom: 5%;">
					<form action=resetPassword.php method="post">
				<?php
    if ($resetPassword_error_message != "") {
        echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $resetPassword_error_message . '</div>';
    }
    ?>
		<table>
							<tr>
								<td><label for="userInput">Username / email: </label></td>
								<td><input id="userInput" name="userInput" type="text"
									style="width: 155%"></td>
							</tr>
						</table>
						<input type="submit" value="Submit" name="resetPasswordForm"
							class="btn btn-outline-success"
							style="width: 220px; font-weight: bold;" /> <a href="index.php"
							class="btn btn-outline-primary"
							style="width: 220px; font-weight: bold;">Home</a>
					</form>
				</div>
			</div>
		</div>
		<div style="height: 5%; color: white;">
			<p>
				Copyright (c) 2018 BIS20303 Web Security <a href="about.php">About</a>
			</p>
		</div>
	</div>
</body>
</html>