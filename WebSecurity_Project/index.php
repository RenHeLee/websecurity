<?php
/**
 * index.php
 *
 * Main file
 *
 * @version    1.0 2018-10-05
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">


<title>Pilot project of Web Security</title>
</head>

<body>
	<div class="container-fluid"
		style="background-image: url('img/img_0001.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
		<div style="height: 30%; color: blue;">
			<h1 style="text-align: center;">Welcome to Markybook</h1>
			<h6 style="text-align: center;">(Project of Web Security BIS20303
				Project)</h6>
		</div>

		<div style="height: 65%;">
			<div class="card"
				style="border-color: #81D8D0; width: 440px; margin-left: auto; margin-right: auto; background: rgba(230, 230, 250, 0.85);">
				<div class="card-block"
					style="margin-left: 5%; margin-right: 5%; margin-top: 5%; margin-bottom: 5%;">
						<?php require 'login.php';?>
				</div>
			</div>
			<p></p>
		</div>


		<div style="height: 5%; color: white;">
			<p>
				Copyright (c) 2018 BIS20303 Web Security <a href="about.php">About</a>
			</p>
		</div>
	</div>
</body>

</html>