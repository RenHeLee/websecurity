<?php
/**
 * changePassword.php
 *
 * changePassword file
 *
 * @version    1.1 2018-11-15
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Start Session
session_start();

// check user request
if (empty($_SESSION['user_input'])) {
    header("Location: index.php");
}

// Application library ( with DemoLib class )
include 'library.php';
$app = new Library();

$userId = $app->getUser_id($_SESSION['user_input']); // get user_id
$userSecureQuestion = $app->getSecurequestion($userId); // get user secure question

$changePassword_error_message = '';

// check change password request
if (! empty($_POST['changePasswordForm'])) {
    $systemSecureAnswer = $app->getSecureanswer($userId);
    $userSecureAnswer = $_POST['secureAnswer'];
    $password = $_POST['password'];
    $verifyPassword = $_POST['verifyPassword'];
    // check user answer
    if ($app->isHashedValue($userSecureAnswer, $systemSecureAnswer)) {
        $changePassword_error_message = 'Invalid details!';
    } else if ($app->isPassword($password, $verifyPassword)) {
        $changePassword_error_message = 'Password are not same!';
    } else {
        $app->updatePassword($userId, $password); // check user login
        $_SESSION['user_id'] = $userId;
        header("Location: profile.php"); // Redirect user to the profile.php
    }
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>Forget password</title>
</head>
<body>
	<div class="container-fluid"
		style="background-image: url('img/img_0005.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
		<div style="height: 15%; color: blue">
			<h1 style="text-align: center">Change Password</h1>
		</div>

		<div style="height: 80%;">
			<div class="card"
				style="border-color: #81D8D0; width: 550px; margin-left: auto; margin-right: auto; margin-top: auto; margin-bottom: auto; background: rgba(230, 230, 250, 0.7);">
				<div class="card-block"
					style="padding-left: 5%; padding-right: 5%; padding-top: 5%; padding-bottom: 5%;">
					<form action=changePassword.php method="post">
		<?php
if ($changePassword_error_message != "") {
    echo '<div class="alert alert-danger"><strong>Error: </strong> ' . $changePassword_error_message . '</div>';
}
?>
		<table>
							<tr>
								<td>
				<?php
    echo $userSecureQuestion . ": ";
    ?>
				</td>
								<td><input id="secureAnswer" name="secureAnswer" type="text"
									style="width: 400px"></td>
							</tr>

							<tr>
								<td><label for="password">Password: </label></td>
								<td><input id="password" name="password" type="password"
									pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}"
									title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"
									style="width: 400px"></td>
							</tr>
							<tr>
								<td><label for="verifyPassword">Verify password: </label></td>
								<td><input id="verifyPassword" name="verifyPassword"
									type="password" style="width: 400px"></td>
							</tr>
						</table>
						<input type="submit" value="Login" name="changePasswordForm"
							class="btn btn-outline-success"
							style="width: 49%; font-weight: bold;" /> <a href="index.php"
							class="btn btn-outline-primary"
							style="width: 49%; font-weight: bold;">Home</a>
					</form>

				</div>
			</div>
		</div>
		<div style="height: 5%; color: black;">
			<p>
				Copyright (c) 2018 BIS20303 Web Security <a href="about.php">About</a>
			</p>
		</div>
	</div>
</body>
</html>