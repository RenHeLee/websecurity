<?php

/**
 * database.php
 *
 * Manage PDO object
 *
 * @version    1.0 2018-10-15
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018 
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */


try {
    $pdo = new PDO("mysql:host = '127.0.0.1:3306'; dbname = 'WebSecurity'", 'root', '');
    // set the PDO error mode to exception
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "";
} catch (PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}
?>