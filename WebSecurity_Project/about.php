<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>About</title>
</head>
<body>
	<div class="container-fluid"
		style="background-image: url('img/img_0003.jpg'); background-position: center; background-repeat: no-repeat; background-size: cover;">
		<div style="height: 15%; color: white;">
			<h1 style="text-align: center;">About of Markybook</h1>
		</div>

		<div style="height: 85%;">
			<div class="card"
				style="border-color: #81D8D0; overflow-y: scroll; height: 90%; width: 50%; margin-left: auto; margin-right: auto; background: rgba(230, 230, 250, 0.85);">
				<div class="card-block"
					style="padding-left: 5%; padding-right: 5%; padding-top: 5%; padding-bottom: 5%;">
					<p>version 1.0 2018-10-05</p>
					<p>version 1.1 2018-10-18</p>
					<p>version 1.1.1 2018-10-19</p>
					<p>version 1.1.1 2018-10-20</p>
					<p>version 1.2 2018-11-07</p>
					<p>version 1.2 2018-11-08</p>
					<p>version 1.2 2018-11-15</p>
					<p>WebSecurity_Project</p>
					<p>Copyright (c) 2018</p>
					<p>GNU General Public License</p>
					<p>Since Release 1.0</p>
					<a href="index.php" class="btn btn-outline-primary"
						style="width: 32%; font-weight: bold;">Home</a>
				</div>
			</div>
		</div>
	</div>


</body>
</html>