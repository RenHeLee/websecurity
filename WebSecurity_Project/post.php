<?php
/**
 * post.php
 *
 * post file
 *
 * @version    1.1 2018-11-07
 * @package    WebSecurity_Project
 * @copyright  Copyright (c) 2018
 * @license    GNU General Public License
 * @since      Since Release 1.0
 */

// Start Session
session_start();

// check user login
if (empty($_SESSION['user_id'])) {
    header("Location: index.php");
}

// Application library ( with DemoLib class )
include 'library.php';
$app = new Library();
$user = $app->userDetails($_SESSION['user_id']); // get user details
$post;
$post = $app->getPost($_SESSION['user_id']); // get user post

if (! empty($_POST['newPostForm'])) {
    $time = date("Y-m-d H:i:s");
    $app->uploadPost($_SESSION['user_id'], $_POST['newPost'], $time);
    header("Location: post.php");
}

?>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<link rel="stylesheet" href="css/bootstrap-grid.css">
<link rel="stylesheet" href="css/bootstrap-grid.min.css">
<link rel="stylesheet" href="css/bootstrap-reboot.css">
<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<title>New post</title>
</head>

<body>
	<div class="container-fluid" style="background-color: lightcyan;">
		<div style="height: 83%; overflow-y: scroll;">
			<h3>Post</h3>			
		<?php
if (is_null($post)) {
    echo "Haven't post anything yet...";
} else {

    echo $app->displayPost($user->username, $post);
}
?></div>
		<div style="height: 2%;"></div>
		<div style="height: 15%;">
			<form action=post.php method="post">
				<textarea rows="10" cols="30" name="newPost" wrap="hard"
					placeholder="What's on your mind, <?php echo $user->username; ?>"
					maxlength="5000" style="width: 100%; height: 60%;" required></textarea>
				<input type="submit" value="Submit" name="newPostForm"
					class="btn btn-outline-primary"
					style="width: 7%; margin-top: 3px; margin-left: 93%; font-weight: bold;" />
			</form>
		</div>
	</div>
</body>
</html>